import RPi.GPIO as gpio
import time

def setBoardAndPins(opins=[],ipins=[]):
	gpio.setmode(gpio.BOARD)
	if opins:
		gpio.setup(opins,gpio.OUT)
	if ipins:
		gpio.setup(ipins,gpio.IN)

def pulse(pin,maxtime):
	count = 0
	updelta = 0.0003
	delta = 0.01
	while count < maxtime:
		for i in xrange(10):
			gpio.output(pin,True)
			time.sleep(updelta)
			gpio.output(pin,False)
			time.sleep(delta)
		count +=1
		delta = delta*0.98
		print delta
		if delta<0.0001:
			print esco
			return
			
def backandforth(pin,maxtime,dimfactor = 0.90):
	count = 0
	updelta = 0.0003
	startingdelta = 0.005
	delta = startingdelta
	#dimfactor = 0.98
	revertpoint= updelta/10
	while count < maxtime:
		for i in xrange(10):
			gpio.output(pin,True)
			time.sleep(updelta)
			gpio.output(pin,False)
			time.sleep(delta)
		#count +=1
		delta = delta*dimfactor
		print delta
		if delta<revertpoint:
			print "revert"
			dimfactor = 2-dimfactor
			count +=1
		if delta>startingdelta:
			print "revert"
			dimfactor = 2-dimfactor
			count +=1
			#print "esco pin ",pin 
			#return
			
			
	
def intermit(pin,delta,count): 
	print pin, " ", count
	for i in xrange(count):
		print pin, " ", count, " ", i
		gpio.output(pin,True)
		time.sleep(delta/2)
		gpio.output(pin,False)
		time.sleep(delta/2)
	
